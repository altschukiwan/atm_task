package com.alchuk.task.security.authProvider;

import io.jsonwebtoken.Claims;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public interface AuthProvider {
    Authentication createAuth(Claims claims);
}
