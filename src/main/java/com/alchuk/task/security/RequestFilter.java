package com.alchuk.task.security;

import com.alchuk.task.security.authProvider.AuthProvider;
import com.alchuk.task.security.tokenService.AccessTokenService;
import com.alchuk.task.security.util.JwtTokenUtil;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class RequestFilter extends OncePerRequestFilter {

    private static final List<String> EXCLUDED_URLS = Arrays.asList();

    private final AccessTokenService tokenService;
    private final AuthProvider authProvider;

    public RequestFilter(AccessTokenService tokenService, AuthProvider authProvider) {
        this.tokenService = tokenService;
        this.authProvider = authProvider;

    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return EXCLUDED_URLS.stream()
                .anyMatch(url -> request.getRequestURI().startsWith(url));
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String jwt = JwtTokenUtil.extractTokenFromHeader(request);
        Claims claims = tokenService.decodeJWT(jwt);
        Authentication auth = authProvider.createAuth(claims);
        SecurityContextHolder.getContext().setAuthentication(auth);
        filterChain.doFilter(request, response);
    }
}
