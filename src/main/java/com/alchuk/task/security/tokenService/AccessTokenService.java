package com.alchuk.task.security.tokenService;

import io.jsonwebtoken.Claims;
import org.springframework.stereotype.Service;

@Service
public interface AccessTokenService {

    String createJWT();

    Claims decodeJWT(String jwt);
}
