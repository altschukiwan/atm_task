package com.alchuk.task.security.util;

import com.alchuk.task.InvalidAuthorizationHeaderException;
import com.alchuk.task.MissingAuthorizationHeaderException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public  class JwtTokenUtil {

    private final static String JWT_HEADER_PREFIX = "Bearer ";

    public static String extractTokenFromHeader(HttpServletRequest request) {
        Optional<String> bearerToken = Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION));
        return bearerToken
                .map(JwtTokenUtil::extractTokenWithoutPrefix)
                .orElseThrow(MissingAuthorizationHeaderException::new);
    }

    private static String extractTokenWithoutPrefix(String bearerToken) {

        String tokenWithoutPrefix = null;

        if (isValidToken(bearerToken)) {
            tokenWithoutPrefix = bearerToken.substring(JWT_HEADER_PREFIX.length());
        } else {
            throw new InvalidAuthorizationHeaderException();
        }

        return tokenWithoutPrefix;
    }

    private static Boolean isValidToken(String token) {
        return StringUtils.length(token) > JWT_HEADER_PREFIX.length() && token.startsWith(JWT_HEADER_PREFIX);
    }
}
