package com.alchuk.task.constants;

public final class ErrorMessage {

    //auth
    public final static String MISSING_AUTHORIZATION_HEADER = "MISSING_AUTHORIZATION_HEADER";
    public final static String INVALID_AUTHORIZATION_HEADER = "INVALID_AUTHORIZATION_HEADER";
}
