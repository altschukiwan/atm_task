package com.alchuk.task.card;

import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(collectionResourceRel = "cards", path = "cards")
public interface CardRepository extends Repository<CardDao, Long> {

    @RestResource
    CardDao findById(Long id);

}
