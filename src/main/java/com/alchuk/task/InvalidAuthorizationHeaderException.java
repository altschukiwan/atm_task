package com.alchuk.task;

import static com.alchuk.task.constants.ErrorMessage.INVALID_AUTHORIZATION_HEADER;

public class InvalidAuthorizationHeaderException extends RuntimeException{
    public InvalidAuthorizationHeaderException() {
        super(INVALID_AUTHORIZATION_HEADER);
    }
}
