package com.alchuk.task;

import static com.alchuk.task.constants.ErrorMessage.MISSING_AUTHORIZATION_HEADER;

public class MissingAuthorizationHeaderException extends RuntimeException{
    public MissingAuthorizationHeaderException() {
        super(MISSING_AUTHORIZATION_HEADER);
    }
}
